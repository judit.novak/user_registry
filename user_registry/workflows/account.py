"""Business Logic relating to accounts"""

from user_registry.external.communication_interface import send_user_validation_code
from user_registry.core.model_dataclasses import UserAccountValidationCode
from user_registry.core.models import UserAccount


def account_creation_workflow(account: UserAccount):
    """User creation workflow"""
    # Create account
    account.create()

    # Request validation code
    validation_code = account.generate_validation_code()

    # Send the validation code to the user
    send_user_validation_code(account.username.address, validation_code)


def account_validation_workflow(account: UserAccount, validation_code: UserAccountValidationCode):
    """User validation workflow"""
    account.validate(validation_code)
