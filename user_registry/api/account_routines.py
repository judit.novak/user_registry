from werkzeug.datastructures import Authorization

from user_registry.core.model_dataclasses import (UserAccountEmail,
                                                  UserAccountPassword,
                                                  UserAccountValidationCode)
from user_registry.core.models import UserAccount


def authenticate(authorization: Authorization) -> UserAccount:
    """Authenticate a user based on credentials from the authorization headers"""

    # Mypy doesn't know that we already checked that required.authorization is not empty
    username = UserAccountEmail(authorization.get('username'))     # type: ignore[arg-type]
    password = UserAccountPassword(authorization.get('password', ''))  # type: ignore[arg-type]

    account = UserAccount(username, password)
    account.authenticate()
    return account


def account_from_payload(payload_data: dict) -> UserAccount:

    # Type hinting hinted to be removed:
    # https://stackoverflow.com/questions/68436658/mypy-says-request-json-returns-optionalany-how-do-i-solve
    username = UserAccountEmail(payload_data.get('username', ''))       # type: ignore[union-attr]
    password = UserAccountPassword(payload_data.get('password', ''))    # type: ignore[union-attr]

    return UserAccount(username, password)


def code_from_payload(payload_data: dict) -> UserAccountValidationCode:
    validation_code_raw = payload_data.get('validation_code', '')           # type: ignore[union-attr]
    return UserAccountValidationCode(validation_code_raw)
