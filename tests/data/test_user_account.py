from datetime import datetime

import pytest

from user_registry.data.exceptions import (UserRegistryDBDoesntExist,
                                           UserRegistryDBException)
from user_registry.data.user_account import (add_validation_code, create_user,
                                             get_user, verify_validation_code)

FREEZE_TIME = '2017-05-21 12:12:00'


def test_create_user(app):
    with app.app_context():
        assert create_user('toto@example.com', 'secret')


def test_create_user_unique(app):
    with app.app_context():
        create_user('toto@example.com', 'secret')

        with pytest.raises(UserRegistryDBException) as err:
            create_user('toto@example.com', 'secret')

        assert str(err.value) == (
            'Duplicated entry (psycopg2.errors.UniqueViolation) duplicate key value violates '
            'unique constraint "user_accounts_email_key"\n'
            'DETAIL:  Key (email)=(toto@example.com) already exists.\n\n'
            "[SQL: SET LOCAL log_min_error_statement='PANIC';"
            "INSERT INTO user_accounts (EMAIL, PASSWORD) VALUES ('toto@example.com', sha512('secret'));]\n"
            '(Background on this error at: https://sqlalche.me/e/14/gkpj)'
        )


def test_get_user(app):
    email = 'toto@example.com'
    password = 'secret'
    time_before = datetime.utcnow()

    with app.app_context():
        create_user(email, password)

        user_data = get_user('toto@example.com')
        assert user_data['email'].strip() == email
        assert not user_data['validated_at']
        assert user_data['password_hash']
        assert user_data['created_at'] > time_before
        assert user_data['created_at'] < datetime.utcnow()


def test_get_user_doesnt_exist(app):
    nonexistent_email = 'nonexisting@example.com'
    with app.app_context():
        with pytest.raises(UserRegistryDBDoesntExist) as err:
            get_user(nonexistent_email)

        assert str(err.value) == (f"User '{nonexistent_email}' not found")


def test_add_validation_code(app):
    email = 'toto@example.com'

    with app.app_context():
        create_user(email, 'secret')
        # No exception raised
        assert add_validation_code(email, '1234')


def test_verify_validation_code(app):
    email = 'toto@example.com'
    code = '1234'
    time_before = datetime.utcnow()

    with app.app_context():
        create_user(email, 'secret')
        add_validation_code(email, code)
        verify_validation_code(email, code)
        user_data = get_user(email)

    assert user_data['validated_at'] > time_before
    assert user_data['validated_at'] < datetime.utcnow()


def test_verify_validation_code_expired(app, mocker):
    mocker.patch('user_registry.data.user_account.APP_NEW_ACCOUNT_VALIDATION_MINUTES', 0)

    email = 'toto@example.com'
    code = '1234'

    with app.app_context():
        create_user(email, 'secret')
        add_validation_code(email, code)
        verify_validation_code(email, code)
        user = get_user(email)

    assert not user.validated_at
