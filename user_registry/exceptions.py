import logging

from flask import jsonify
from werkzeug.exceptions import HTTPException

logger = logging.getLogger(__name__)


class UserRegistryException(Exception):
    pass


def user_registry_handler(error: Exception):
    try:
        # NOTE Mypy doesn't understand the point of this "ask-forgiveness" structure
        message = error.message                 # type: ignore[attr-defined]
    except AttributeError:
        message = str(error)
    logger.error(f"{message}")
    response = jsonify(message=message)
    response.status_code = (
        error.code if isinstance(error, HTTPException) else 500     # type: ignore[assignment]
    )
    return response
