from flask import current_app

from user_registry.api.user_account_endpoints import bp as bp_api


def add_routes():
    current_app.register_blueprint(bp_api)
