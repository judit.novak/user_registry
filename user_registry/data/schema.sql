CREATE TABLE IF NOT EXISTS user_accounts (
    id              SERIAL      PRIMARY KEY,
    email           VARCHAR     NOT NULL    UNIQUE,
    password        BYTEA       NOT NULL,
    created_at      TIMESTAMP   NOT NULL    DEFAULT NOW(),
    validated_at    TIMESTAMP   DEFAULT NULL
);


CREATE TABLE IF NOT EXISTS validation_code (
    id                  SERIAL      PRIMARY KEY,
    account_id          INT         NOT NULL    UNIQUE,
    validation_code     CHAR(4)     NOT NULL,
    created_at          TIMESTAMP   NOT NULL    DEFAULT NOW(),

    CONSTRAINT fk_user_account
      FOREIGN KEY(account_id)
	  REFERENCES user_accounts(id)
);
