import itertools
import random
import re
import string
from dataclasses import dataclass, field
from typing import Pattern

from user_registry.constants import (APP_USER_ACCOUNT_PW_LENGTH,
                                     APP_VALIDATION_CODE_LENGHT)
from user_registry.core.exceptions import (UserRegistryEmailException,
                                           UserRegistryValidationCodeException)


@dataclass
class UserAccountEmail:
    """Validation and Unification of data.
       Standardization is required to avoid inconsistencies (for exampe the
       same email with differenct case resulting in a duplicated object
       creation)
    """

    address: str

    def __post_init__(self):
        if not re.match(r'[\w\.-]+@[\w\.-]+\.[\w-]+$', self.address):
            raise UserRegistryEmailException(f"Invalid email: '{self.address}'")
        self.address = self.address.lower()


@dataclass
class UserAccountPassword:
    """Password field
        - at least 8 chars lenght
        - no control chars allowed
    """

    password: str

    _control_chars: str = field(
            init=False,
            repr=False,
            default=''.join(map(chr, itertools.chain(range(0x00, 0x20), range(0x7f, 0xa0))))
    )
    _pw_regex: Pattern[str] = field(init=False, repr=False, default=re.compile('[%s]' % re.escape(str(_control_chars))))

    def __post_init__(self):
        if len(self.password) < APP_USER_ACCOUNT_PW_LENGTH:
            msg = f"Invalid password, must be at least {APP_USER_ACCOUNT_PW_LENGTH} characters lenght"
            raise UserRegistryEmailException(msg)
        if not re.search(self.__class__._pw_regex, self.password):
            msg = "Invalid password, control characters found inside"
            raise UserRegistryEmailException(msg)


@dataclass
class UserAccountValidationCode:
    """Validation and Unification of data.
    """

    code: str = field(
            default_factory=lambda: ''.join(random.choices(string.digits, k=APP_VALIDATION_CODE_LENGHT))
    )

    def __post_init__(self):
        self.validate(self.code)

    @staticmethod
    def validate(instr: str):
        if len(instr) != APP_VALIDATION_CODE_LENGHT or not instr.isdigit():
            msg = f"Invalid code '{instr}', must be {APP_VALIDATION_CODE_LENGHT} digits"
            raise UserRegistryValidationCodeException(msg)
