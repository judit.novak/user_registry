import json

import pytest

from user_registry.data.user_account import get_user


class MockValidationCode:
    code = '1234'

    def __init__(self, *args, **kwargs):
        pass


def test_allowed_methods(app, client, mocker, mock_external_comm_request):

    mocker.patch('user_registry.core.models.UserAccountValidationCode', new=MockValidationCode)

    username = 'x@y.z'
    password = 'blablabla'
    resp = client.post(
        '/user_registry/user/register',
        content_type='application/json',
        data=json.dumps({
            'username': username,
            'password': password
        })
    )
    assert resp.status_code == 201

    resp = client.post(
        '/user_registry/user/validate',
        content_type='application/json',
        auth=(username, password),
        data=json.dumps({
            'username': username,
            'validation_code': '1234'
        })
    )
    assert resp.status_code == 200

    with app.app_context():
        user_data = get_user(username)
        assert user_data.validated_at


def test_cant_create_the_same_user_twice(client, mock_external_comm_request):
    user_data = {'username': 'x@y.z', 'password': 'blablabla'}
    resp = client.post(
        '/user_registry/user/register',
        content_type='application/json',
        data=json.dumps(user_data)
    )
    resp = client.post(
        '/user_registry/user/register',
        content_type='application/json',
        data=json.dumps(user_data)
    )
    assert resp.status_code == 409
    assert resp.json['message'] == f"User '{user_data['username']}' already exists"


def test_register_input_required(client, mock_external_comm_request):
    resp = client.post(
        '/user_registry/user/register',
        content_type='application/json',
        data=json.dumps({})
    )
    assert resp.status_code == 422
    assert resp.json['message'] == "Username and password input is required"


def test_validate_input_required(client, mock_external_comm_request, account_with_pw, password):
    resp = client.post(
        '/user_registry/user/validate',
        content_type='application/json',
        auth=(account_with_pw.username.address, password.password),
        data=json.dumps({})
    )
    assert resp.status_code == 422
    assert resp.json['message'] == 'Validation code has to be passed in the payload'


@pytest.mark.parametrize('code_str', [
    ('grrrrrr'),
    ('12g4'),
    ('12345'),
])
def test_validate_fails_invalid_code(
        code_str,
        app, client, mock_external_comm_request, account_with_pw, password
):
    resp = client.post(
        '/user_registry/user/validate',
        content_type='application/json',
        auth=(account_with_pw.username.address, password.password),
        data=json.dumps({
            'validation_code': code_str
        })
    )
    assert resp.status_code == 422
    assert resp.json['message'] == f"Invalid code '{code_str}', must be 4 digits"

    with app.app_context():
        user_data = get_user(account_with_pw.username.address)
        assert not user_data.validated_at


def test_validate_fails_wrong_password(
        app, client, mock_external_comm_request, account_with_pw, password
):
    resp = client.post(
        '/user_registry/user/validate',
        content_type='application/json',
        auth=(account_with_pw.username.address, 'blablablbla'),
        data=json.dumps({
            'validation_code': '1234'
        })
    )
    assert resp.status_code == 401
    assert resp.json['message'] == f"Password mismatch for user '{account_with_pw.username.address}'"

    with app.app_context():
        user_data = get_user(account_with_pw.username.address)
        assert not user_data.validated_at


def test_validate_fails_wrong_code(
        app, client, mock_external_comm_request, account_with_pw, password
):
    resp = client.post(
        '/user_registry/user/validate',
        content_type='application/json',
        auth=(account_with_pw.username.address, password.password),
        data=json.dumps({
            'validation_code': '1234'
        })
    )
    assert resp.status_code == 400
    assert resp.json['message'] == f"User validation failed for '{account_with_pw.username.address}'"

    with app.app_context():
        user_data = get_user(account_with_pw.username.address)
        assert not user_data.validated_at


@pytest.mark.parametrize('email_str', [
    (''),
    (None),
])
def test_validate_fails_empty_username(
        email_str,
        client, mock_external_comm_request
):
    resp = client.post(
        '/user_registry/user/validate',
        content_type='application/json',
        auth=(email_str, 'somepass'),
        data=json.dumps({
            'validation_code': '1234'
        })
    )
    assert resp.status_code == 500
    assert resp.json['message'] == f"Invalid email: '{email_str}'"


def test_validate_fails_user_doesnt_exist(client, mock_external_comm_request):

    resp = client.post(
        '/user_registry/user/validate',
        content_type='application/json',
        auth=('blablabla', 'somepw'),
        data=json.dumps({
            'validation_code': '1234'
        })
    )
    assert resp.status_code == 500
