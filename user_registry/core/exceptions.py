from user_registry.exceptions import UserRegistryException


class UserRegistryUserAccountException(UserRegistryException):
    pass


class UserRegistryUserAccountDuplicateError(UserRegistryUserAccountException):
    pass


class UserRegistryUserAccountUnauthenticated(UserRegistryUserAccountException):
    pass


class UserRegistryEmailException(UserRegistryException):
    pass


class UserRegistryPasswordException(UserRegistryException):
    pass


class UserRegistryValidationCodeException(UserRegistryException):
    pass
