from hashlib import sha512
from typing import Any

from sqlalchemy.engine.row import LegacyRow  # type: ignore[import]

from user_registry.core.exceptions import (
    UserRegistryUserAccountException,
    UserRegistryUserAccountDuplicateError,
    UserRegistryUserAccountUnauthenticated,
)
from user_registry.core.model_dataclasses import (UserAccountEmail,
                                                  UserAccountPassword,
                                                  UserAccountValidationCode)
from user_registry.data.exceptions import UserRegistryDBException, UserRegistryDBDuplicate
from user_registry.data.user_account import (add_validation_code, create_user,
                                             get_user, verify_validation_code)


class RawDBDataTransformer:
    """Base conversion from raw data to expected Data Model input"""

    @staticmethod
    def transform_raw_to_bool(instuff: Any) -> bool:
        return bool(instuff)


class UserAccount(RawDBDataTransformer):
    __slots__ = ('username', 'password', 'password_hash', 'validated')

    def __init__(
            self,
            username: UserAccountEmail,
            password: UserAccountPassword = None,
            password_hash: bytes = None,
            validated: bool = False
    ) -> None:
        self.username = username
        self.password = password
        self.password_hash = password_hash
        self.validated = validated

    @classmethod
    def transform_raw_user_data(cls, user_data: LegacyRow) -> "UserAccount":
        validated = cls.transform_raw_to_bool(user_data['validated_at'])
        return UserAccount(
            username=UserAccountEmail(user_data['email']),
            password_hash=user_data['password_hash'],
            validated=validated
        )

    @classmethod
    def get_user(cls, username: UserAccountEmail) -> "UserAccount":
        """Retrieve user object from raw data"""
        user_data = get_user(username.address)
        return cls.transform_raw_user_data(user_data)

    def _validate_new(self):
        """Validation mechanism for new model objects that are not yet supposed to exist in the DB"""
        if self.validated:
            raise UserRegistryUserAccountException(f"New user '{self.username.address}' appears as alraedy verified")
        if not self.password:
            raise UserRegistryUserAccountException(f"Password is required for new user '{self.username.address}'")

    def create(self):
        """Save user in case passes validation """
        self._validate_new()
        try:
            # NOTE: Mypy doesn't know, but thanks to validation above, we're safe to refer to the password here
            create_user(self.username.address, self.password.password)      # type: ignore[union-attr]
        except UserRegistryDBDuplicate:
            raise UserRegistryUserAccountDuplicateError(f"User '{self.username.address}' already exists")
        except UserRegistryDBException:
            raise UserRegistryUserAccountException(f"Couldn't save user '{self.username.address}'")
        self._refresh_from_db()

    def _refresh_from_db(self):
        """Get most accurate DB representation of self
           NOTE: It password field wasn't empty, it will become after.
                 However, it's for the best, as normally it's not needed after this call.
                 And we'd rather don't have clear text pw-s hanging around
        """
        new_self = self.__class__.get_user(self.username)
        if not new_self:
            raise UserRegistryUserAccountException(f"User '{self.username.address}' doesn't exist")

        # We can't re-assing self, so we reassign all fields one-by-one
        for attr in self.__slots__:
            setattr(self, attr, getattr(new_self, attr))

    def generate_validation_code(self) -> UserAccountValidationCode:
        """Create a validation code for the user, that's valid for a minute"""
        validation_code = UserAccountValidationCode()
        add_validation_code(self.username.address, validation_code.code)
        return validation_code

    def validate(self, validation_code: UserAccountValidationCode):
        """Set the user as validated, in case code matches and didn't yet expire"""
        if self.validated:
            raise UserRegistryUserAccountException(f"User '{self.username.address}' already validated")
        verify_validation_code(self.username.address, validation_code.code)
        self._refresh_from_db()

    def authenticate(self):
        """Authenticate user against the password hash associated with him/her in the DB"""

        if not self.password:
            raise UserRegistryUserAccountException(f"Can't authenticate '{self.username.address}' without a password")

        # Mypy doesn't know that we've just checked
        check_pw_hash = sha512(self.password.password.encode('utf-8'))         # type: ignore[union-attr]

        if not self.password_hash:
            self._refresh_from_db()

        if not self.password_hash:
            raise UserRegistryUserAccountException(f"No password found for user '{self.username.address}'")

        if self.password_hash != check_pw_hash.hexdigest():
            raise UserRegistryUserAccountUnauthenticated(f"Password mismatch for user '{self.username.address}'")
