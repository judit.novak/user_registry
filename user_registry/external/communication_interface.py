"""Communication: high-level interface modules"""

import logging

from user_registry.external.communication_backend import send_code_to_communications_server


logger = logging.getLogger(__name__)


def send_user_validation_code(email, validation_code):
    return send_code_to_communications_server(email, validation_code)
