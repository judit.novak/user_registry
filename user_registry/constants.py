# Password required lenght
APP_USER_ACCOUNT_PW_LENGTH = 8

# Validation interval for new accounts
APP_NEW_ACCOUNT_VALIDATION_MINUTES = 1

# Validation code length
APP_VALIDATION_CODE_LENGHT = 4
