﻿# user_registry

A very simple Flask user registry application.

In this document we provide a User's Guide. For those
who may be interested Developer's Doc or Design Document, we recommend the
[Technical Documentation](./docs/tech_doc.md).


## Description

**user_registry** is a small application, primarily dedicated to store basic
user account data, with initial interactive validation from the user's side.

Functionality is exposed via two web API endpoints.

## Getting started

The application is fully dockerized, thus a working installation is pretty much
the only dependency required.

Services include the database (we use Postgres), and an application
server that's also running the programmatic API interface.

The application responds to two requests: one for user creation, and second is
to verify the validity of an already existing user via a generated code.

The complete cycle goes as the following. The user contacts user creation API
endpoint with a name and a password. On a successful return, user data gets
registered, and the user very soon receives a random 4-digit verification code.
The user had a minute to contact the user validation endpoint with the code
received, which would change the user's status to verified.


## Installation

Assuming that you have a Docker installed, the best way to build and run the
application is to check out the application source code from Git, build the
image(s) and run it all in Docker containers.

Before building, there are a few configuration variables to be defined.
For some of them we may provide convenient default values, however typically
sensitive information like passwords should be really defined locally.
In the project root directory you find a `.env_sample` file. You should rename
this to `.env`, and change any configuration according to your preference.

The list of settings essential both for the database and the application
service are the following:


|**Variable**             | Value             |   Required    |    Default    |
| ----------------------- |:------------------|:-------------:|:-------------:|
| DB_PROTOCOL             | database protocol | **mandatory** | postgresql    |
| DB_USER                 | database user     | **mandatory** | user_registry |
| DB_PASSWORD             | database password | **mandatory** |               |
| DB_NAME                 | database name     | **mandatory** | user_registry |
| DB_HOST                 | database host     | **mandatory** | localhost     |
| DB_PORT                 | database port     | **mandatory** | 5432          |


Once all available in `.env`, you are ready to build the new application image.
Simple `make` commands are provided, thus all you need to do is:

```
    make app-build
    make app-up
```
NOTE: The API server may fail on your first, initial `make app-up` command.
The reason is a DB server restart after the initial database creation. If the API
server tries to contact it in the wrong moment, during its own init, the init
it may fail and shut down. Don't worry, just run `make app-up` again.

In case you may want to run the tests, similarly you should run

```
    make test-build
    make test-run
```


## Usage

The service is available via the following to API endpoints.

### Adding a new user

```
POST user_registry/user/register
```
The expected payload is the username and the password, such as:
```
{
    "username": "<username>",
    "password": "<password>"
}
```
where

 - the `username` has to be a valid email address
 - the `password` has to be an at least 8 character long string of
   (non-control) characters

As a part of the user registration workflow, once user data was successfully
saved, a 4-digit code is generated and communicated to the user (see: [External
Dependencies](#external-dependencies)). The user has to respond within a minute
delay.


### Validating a user

The second endpoint is 

```
POST user_registry/user/validate
```
The expected payload is the username and the password, such as:
```
{
    "validation_code": "<4-digit-code>"
}
```

The request has to include Basic Authenticatoion headers and credentials,
as authentication is required for this endpoint..

### Simple example usage

```
$ cat curl_commands.sh 
PASSWORD="blablabla"

user_registry_adduser () {
    curl -X POST http://localhost:8080/user_registry/user/register \
        -H 'Content-Type: application/json' \
        -d '{"username": "'$1'", "password": "'${PASSWORD}'"}'

    echo -n "Validation code: "
    read -r CODE

    curl -X POST http://localhost:8080/user_registry/user/validate \
        -H 'Content-Type: application/json' \
        --user "$1:${PASSWORD}" \
        -d '{"validation_code": "'$CODE'"}'
}

$ . curl_commands.sh
$ user_registry_adduser me@example.com
{"message":"me@example.com"}
Validation code: <4-digit validation code from logs>
{"message":"me@example.com"}
```

### External dependencies

In theory the user should be receiving the validation code in an
email. The current version of the application is locally simulating
the behavior, showing the code in the console logs.

## Support

This has been pretty much of a one-person project so far, but I'm sure there
to help providing help or a fix if needed. Feel free to contact :-)

## Roadmap

The next extension on the service, should be to add the functionality to 
reset the validation code on request. Typically for users who may miss
the 1 minute limit, and would like to get a 2nd chance to validate their
accounts.

## Authors and acknowledgment

Judit Novak <judit.novak@gmail.com>
