import re
from hashlib import sha512

import pytest

from user_registry.core.exceptions import (
    UserRegistryUserAccountException, UserRegistryUserAccountUnauthenticated)
from user_registry.core.model_dataclasses import (UserAccountEmail,
                                                  UserAccountPassword,
                                                  UserAccountValidationCode)
from user_registry.core.models import UserAccount
from user_registry.data.exceptions import UserRegistryDBException
from user_registry.data.user_account import get_user


def test_define():
    # Checking if no exception are raised
    email = UserAccountEmail('my_little.pony@example-site.com')
    password = UserAccountPassword('secret12')
    UserAccount(email, password)


def test_create(app):
    email = UserAccountEmail('my_little.pony@example-site.com')
    password = UserAccountPassword('secret12')
    account = UserAccount(email, password)
    with app.app_context():
        # Checking if no exception are raised
        account.create()


def test_create_fails(app):
    email = UserAccountEmail('my_little.pony@example-site.com')
    password = UserAccountPassword('secret12')
    account = UserAccount(email, password)

    # "Hack", this value shouldn't be set manually ever
    account.validated = True

    with app.app_context():
        with pytest.raises(UserRegistryUserAccountException) as err:
            account.create()

    assert str(err.value) == f"New user '{email.address}' appears as alraedy verified"


def test_create_db_fails(app, mocker):
    mocker.patch('user_registry.core.models.create_user', side_effect=UserRegistryDBException)

    email = UserAccountEmail('my_little.pony@example-site.com')
    password = UserAccountPassword('secret12')
    account = UserAccount(email, password)

    with app.app_context():
        with pytest.raises(UserRegistryUserAccountException) as err:
            account.create()
    assert str(err.value) == f"Couldn't save user '{email.address}'"


def test_transform_raw_user_data(app, account_with_pw):
    with app.app_context():
        raw_user = get_user(account_with_pw.username.address)
        user = UserAccount.transform_raw_user_data(raw_user)

    assert raw_user.password_hash == user.password_hash
    assert bool(raw_user.validated_at) == user.validated


def test_get_user(app, email, password):
    account = UserAccount(email, password)
    with app.app_context():
        account.create()
        account_from_db = UserAccount.get_user(email)

    assert account_from_db.username == account.username
    text_pw_sha_hash = sha512(password.password.encode('utf-8'))
    assert account_from_db.password_hash == text_pw_sha_hash.hexdigest()


def test_authenticate(app, email, password):
    account = UserAccount(email, password)
    with app.app_context():
        # We create the account
        account.create()

        # And then we try to log in "from the ourside" again
        account_logging_in = UserAccount(email, password)
        account_logging_in.authenticate()

    assert account_logging_in.username.address == email.address
    assert account_logging_in.password_hash == account.password_hash


def test_authenticate_fails(app, account_with_pw):
    wrongpass = UserAccountPassword('wrongpass')
    with app.app_context():
        # Trying to log in as an existing user with a wrong password
        account_logging_in = UserAccount(account_with_pw.username, wrongpass)

        with pytest.raises(UserRegistryUserAccountUnauthenticated) as err:
            account_logging_in.authenticate()

    assert str(err.value) == f"Password mismatch for user '{account_with_pw.username.address}'"


def test_authenticate_fails_corrupt_data(app, account_with_pw):
    with app.app_context():
        account_logging_in = UserAccount(account_with_pw.username, account_with_pw.password)

        # Hack, this shouldn't happen
        account_logging_in.password = None

        with pytest.raises(UserRegistryUserAccountException) as err:
            account_logging_in.authenticate()

    assert str(err.value) == f"Can't authenticate '{account_with_pw.username.address}' without a password"


def test_generate_validation_code(app, account_with_pw):
    with app.app_context():
        code = account_with_pw.generate_validation_code()
    assert code
    assert isinstance(code, UserAccountValidationCode)
    assert len(code.code) == 4
    assert re.match('[0-9]{4}$', code.code)


def test_validate(app, account_with_pw):
    with app.app_context():
        code = account_with_pw.generate_validation_code()
        account_with_pw.validate(code)
    assert account_with_pw.validated


def test_validate_expires(mocker, app, account_with_pw):
    mocker.patch('user_registry.data.user_account.APP_NEW_ACCOUNT_VALIDATION_MINUTES', 0)
    with app.app_context():
        code = account_with_pw.generate_validation_code()
        account_with_pw.validate(code)
    assert not account_with_pw.validated


def test_validate_wrong_code_not_in_db(mocker, app, account_with_pw):
    mocker.patch('user_registry.data.user_account.APP_NEW_ACCOUNT_VALIDATION_MINUTES', 0)
    wrong_code = UserAccountValidationCode()
    with app.app_context():
        account_with_pw.generate_validation_code()
        account_with_pw.validate(wrong_code)
    assert not account_with_pw.validated


def test_validate_wrong_code_in_db(mocker, app, account_with_pw, password):
    """This is here after a banal bug that allowed to authenticate with someone else's code"""
    mocker.patch('user_registry.data.user_account.APP_NEW_ACCOUNT_VALIDATION_MINUTES', 0)

    email = UserAccountEmail('other_user@example-site.com')
    other_user = UserAccount(email, password)

    with app.app_context():
        other_user.create()
        other_user_code = other_user.generate_validation_code()
        account_with_pw.validate(other_user_code)
    assert not account_with_pw.validated
