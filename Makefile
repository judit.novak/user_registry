# HELP
# This will output the help for each task
# thanks to https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

docker-cmd = docker-compose
docker-volume-cmd = docker volume

testenv = 'testenv'
testdb = 'test-db'
test-docker-cmd = docker-compose -f dockerenv/docker-compose.test.yml --env-file dockerenv/.env_testing


# DOCKER TASKS
# Build the container
app-build: ## Build the container
	$(docker-cmd) build

app-up: ## Run the container
	$(docker-cmd) up

app-down: ## Stop app and related containers
	$(docker-cmd) down

app-db-rm: ## Fully remove the test DB
	$(docker-volume-cmd) rm user_registry_pgdata

test-build: ## Test environment build
	$(test-docker-cmd) build

test-build-all: ## Test environment build
	$(test-docker-cmd) build --no-cache 

test-run: ## Test environment running testss
	- $(test-docker-cmd) run $(testenv)
	$(test-docker-cmd) stop $(testdb)

test-db-up: ## Bring up the test DB, typically for local development testing
	$(test-docker-cmd) start $(testdb)

test-db-rm: ## Fully remove the test DB
	$(docker-volume-cmd) rm dockerenv_pgdata-test

