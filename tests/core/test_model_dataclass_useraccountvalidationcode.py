import re

import pytest

from user_registry.core.exceptions import UserRegistryValidationCodeException
from user_registry.core.model_dataclasses import UserAccountValidationCode


def test_create():
    code = UserAccountValidationCode()
    assert re.match('[0-9]{4}$', code.code)


def test_unique():
    code1 = UserAccountValidationCode()
    code2 = UserAccountValidationCode()
    assert code1 != code2


@pytest.mark.parametrize('code_str', [
    ('grrrrrr'),
    ('12g4'),
    ('12345'),
    ('')
])
def test_assignment(code_str):
    with pytest.raises(UserRegistryValidationCodeException) as err:
        UserAccountValidationCode(code_str)
    assert str(err.value) == f"Invalid code '{code_str}', must be 4 digits"
