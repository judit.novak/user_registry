from user_registry.exceptions import UserRegistryException


class UserRegistryDBException(UserRegistryException):
    pass


class UserRegistryDBUserAlreadyValid(UserRegistryDBException):
    pass


class UserRegistryDBDuplicate(UserRegistryDBException):
    pass


class UserRegistryDBDoesntExist(UserRegistryDBException):
    pass
