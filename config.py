import logging
from os import environ


class Config:
    """Set Flask configuration vars from .env file."""

    # General
    FLASK_DEBUG = environ.get('FLASK_DEBUG', False)

    # Database
    DB_DATABASE = environ.get('DB_DATABASE')
    DB_USER = environ.get('DB_USER')
    DB_PASSWORD = environ.get('DB_PASSWORD')
    DB_PROTOCOL = environ.get('DB_PROTOCOL')
    DB_SERVER = environ.get('DB_SERVER')
    DB_PORT = environ.get('DB_PORT')
    SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI')

    if not SQLALCHEMY_DATABASE_URI:
        SQLALCHEMY_DATABASE_URI = (
                f"{DB_PROTOCOL}://{DB_USER}:{DB_PASSWORD}"
                f"@{DB_SERVER}:{DB_PORT}/{DB_DATABASE}"
        )
    SQLALCHEMY_TRACK_MODIFICATIONS = environ.get(
            'SQLALCHEMY_TRACK_MODIFICATIONS', True
    )   # Just to silence Flask warning
    DB_SCHEMA_SCRIPT = 'user_registry/data/schema.sql'

    # Applications
    PROPAGATE_EXCEPTIONS = environ.get('PROPAGATE_EXCEPTIONS', True)
    APP_LOGLEVEL = environ.get('APP_LOGLEVEL', logging.INFO)

    EXTERNAL_COMMUNICATION_SERVICE = 'http://my_fake_service.com/api'
    EXTERNAL_COMM_SERVICE_ENDPOINTS = {
        'send': '/send_email'
    }
