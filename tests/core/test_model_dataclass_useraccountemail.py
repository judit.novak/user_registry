import pytest

from user_registry.core.exceptions import UserRegistryEmailException
from user_registry.core.model_dataclasses import UserAccountEmail


def test_create():
    addr = 'x@y.z'
    # Checking if no exception are raised
    email = UserAccountEmail(addr)
    assert email
    assert email.address == addr


@pytest.mark.parametrize('email_address', [
    ('inv$#%lid@example-site.com'),
    ('invalidexample-site.com'),
    ('invalid@example-sitecom'),
    ('invalid@example-sitecom   '),
])
def test_validate_fails(email_address):

    with pytest.raises(UserRegistryEmailException) as err:
        UserAccountEmail(email_address)

    assert str(err.value) == f"Invalid email: '{email_address}'"
