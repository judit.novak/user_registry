from flask import Flask

from user_registry.data.base import UserRegistryDataBase
from user_registry.exceptions import (UserRegistryException,
                                      user_registry_handler)
from user_registry.api.routes import add_routes


def create_app(config='config.Config') -> Flask:
    """Construct the core application."""

    import logging

    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config)

    # Highest appreciation to tstringer for
    # https://trstringer.com/logging-flask-gunicorn-the-manageable-way/
    # Turns out (after N hours wasted), that Flask logging configuration is hell when run with Gunicorn
    # Attaching the app.logger to Gunicorn's loghandler solves the issue
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers

    if app.config.get('APP_LOGLEVEL'):
        app.logger.setLevel(app.config['APP_LOGLEVEL'])

    with app.app_context():

        # Crete DB schema if not yet there
        db = UserRegistryDataBase()
        db.init_app_db_schema()

        # Create tables for our models
        add_routes()

        app.register_error_handler(UserRegistryException, user_registry_handler)

        return app
