import responses
from flask import current_app


def mock_calls(fnct):
    @responses.activate
    def wrap(*args, **kwargs):
        base_url = current_app.config['EXTERNAL_COMMUNICATION_SERVICE']
        endpoint = current_app.config['EXTERNAL_COMM_SERVICE_ENDPOINTS']['send']
        url = base_url + endpoint

        responses.add(responses.POST, url, json={}, status=200)

        print("**************************************** Mock service *********************************")
        print(f"       {args}, {kwargs}")
        print("***************************************************************************************")

        return fnct(*args, **kwargs)
    return wrap
