"""Communication Backend: All low-level, 3rd-party specific code goes here"""

import logging

import requests
from flask import current_app

from third_party_simulated.mock_service import mock_calls

logger = logging.getLogger(__name__)


def get_send_endpoint():
    base_url = current_app.config['EXTERNAL_COMMUNICATION_SERVICE']
    endpoint = current_app.config['EXTERNAL_COMM_SERVICE_ENDPOINTS']['send']
    return base_url + endpoint


# This is BAD, and a high priority to fix
# See https://gitlab.com/judit.novak/user_registry/-/issues/1
@mock_calls
def send_code_to_communications_server(email, validation_code):
    url = get_send_endpoint()
    response = requests.post(url, data={'email': email, 'validation_code': validation_code})
    if response.ok:
        return True
    return False
