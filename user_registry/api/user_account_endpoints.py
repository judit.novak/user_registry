import logging
from http import HTTPStatus
from flask import Blueprint, request

import user_registry.api.account_routines as api_account
from user_registry.api.utils import api_return, api_error
from user_registry.core.exceptions import (
    UserRegistryUserAccountUnauthenticated,
    UserRegistryUserAccountDuplicateError,
    UserRegistryValidationCodeException
)
from user_registry.workflows.account import (account_creation_workflow,
                                             account_validation_workflow)

logger = logging.getLogger(__name__)

bp = Blueprint('api', __name__, url_prefix='/')


@bp.route('/user_registry/user/register', methods=['POST'])
def add_user():

    # Input verification
    request_data = request.get_json()
    if not request_data:
        return api_error("Username and password input is required", HTTPStatus.UNPROCESSABLE_ENTITY)

    # Get data from input
    account = api_account.account_from_payload(request_data)                  # type: ignore[arg-type]

    # Invoke corresponding Business Logic
    try:
        account_creation_workflow(account)
    except UserRegistryUserAccountDuplicateError:
        return api_error(f"User '{account.username.address}' already exists", HTTPStatus.CONFLICT)

    # Return on success
    return api_return(account.username.address, HTTPStatus.CREATED, logger)


@bp.route('/user_registry/user/validate', methods=['POST'])
def validate_user():

    # Input verification
    if not request.authorization:
        return api_error("No credentials, authentication is required", HTTPStatus.UNAUTHORIZED, logger)

    # Authentication
    try:
        account = api_account.authenticate(request.authorization)
    except UserRegistryUserAccountUnauthenticated as err:
        return api_error(err, HTTPStatus.UNAUTHORIZED, logger)

    # Get data from input
    request_data = request.get_json()
    if not request_data:
        return api_error("Validation code has to be passed in the payload", HTTPStatus.UNPROCESSABLE_ENTITY, logger)

    try:
        code = api_account.code_from_payload(request_data)
    except UserRegistryValidationCodeException as err:
        return api_error(str(err), HTTPStatus.UNPROCESSABLE_ENTITY, logger)

    # Invoke corresponding workflow
    account_validation_workflow(account, code)               # type: ignore[arg-type]

    # Return depending on success
    if account.validated:
        return api_return(account.username.address, HTTPStatus.OK, logger)
    return api_error(f"User validation failed for '{account.username.address}'", HTTPStatus.BAD_REQUEST, logger)
