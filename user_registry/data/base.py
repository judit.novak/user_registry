from flask import current_app, g
from sqlalchemy import create_engine, text

from user_registry.data.exceptions import UserRegistryDBException


class DBCore:

    def __init__(self, uri, schema_script):
        self.uri = uri
        self.schema_script = schema_script
        self._engine = None

    def get_db_engine(self):
        if not self._engine:
            self._engine = create_engine(self.uri)
        return self._engine

    def init_db(self):
        engine = self.get_db_engine()

        with open(self.schema_script) as schema_script_stream:
            schema_sql = text(schema_script_stream.read())

        with engine.begin() as connection:
            connection.execute(schema_sql)


class UserRegistryDataBase:

    def __init__(self):
        self.app = current_app
        self._db = None

    def _get_new_connection(self):
        if not (self.app.config.get('SQLALCHEMY_DATABASE_URI') and self.app.config.get('DB_SCHEMA_SCRIPT')):
            raise UserRegistryDBException("Can't create DB without configuration and schema")

        self._db = DBCore(
            uri=current_app.config['SQLALCHEMY_DATABASE_URI'],
            schema_script=current_app.config['DB_SCHEMA_SCRIPT']
        )

    def get_app_db(self):
        if not self._db:
            if g.get('db'):
                self._db = g.db
            else:
                self._get_new_connection()
                g.db = self._db
        return self._db

    def init_app_db_schema(self):
        db = self.get_app_db()
        return db.init_db()

    def get_app_db_engine(self):
        db = self.get_app_db()
        return db.get_db_engine()

    def execute(self, sql_text):
        engine = self.get_app_db_engine()
        with engine.begin() as connection:
            return connection.execute(text(sql_text))
