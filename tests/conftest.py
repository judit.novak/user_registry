import pytest
import responses
from flask import current_app

from user_registry import create_app
from user_registry.core.model_dataclasses import (UserAccountEmail,
                                                  UserAccountPassword)
from user_registry.core.models import UserAccount
from user_registry.data.base import UserRegistryDataBase


def cleanup_db():
    """Very simplistic test DB cleanup helper function, given the
       very simple nature or our DB.

       This should be replaced by a more complext yet also more efficient
       mechanism where each test is run within a transaction that could be
       rolled back after.
    """
    cleanup_sql = (
        'DROP TABLE VALIDATION_CODE;'
        'DROP TABLE USER_ACCOUNTS;'
    )
    db = UserRegistryDataBase()
    db.execute(cleanup_sql)
    db.get_app_db_engine().dispose()


@pytest.fixture
def app():
    app = create_app(config='tests.config.Config')
    yield app
    with app.app_context():
        cleanup_db()


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def email():
    return UserAccountEmail('my_little.pony@example-site.com')


@pytest.fixture
def password():
    return UserAccountPassword('secret12')


@pytest.fixture
def account_with_pw(app, email, password):
    with app.app_context():
        account = UserAccount(email, password)
        account.create()
    return account


@pytest.fixture
@responses.activate
def mock_external_comm_request(app):
    with app.app_context():
        base_url = current_app.config['EXTERNAL_COMMUNICATION_SERVICE']
        endpoint = current_app.config['EXTERNAL_COMM_SERVICE_ENDPOINTS']['send']
        endpoint = base_url + endpoint
        responses.add(responses.POST, endpoint, json={}, status=200)
