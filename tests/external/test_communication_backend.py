from user_registry.external.communication_interface import send_user_validation_code


def test_send_user_validation_code(app, mock_external_comm_request):
    with app.app_context():
        assert send_user_validation_code('x@y.z', '1234')
