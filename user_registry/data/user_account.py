from typing import Callable

from sqlalchemy.engine.cursor import LegacyCursorResult  # type: ignore[import]
from sqlalchemy.engine.row import LegacyRow  # type: ignore[import]
from sqlalchemy.exc import IntegrityError, ResourceClosedError, SQLAlchemyError

from user_registry.constants import APP_NEW_ACCOUNT_VALIDATION_MINUTES
from user_registry.data.base import UserRegistryDataBase
from user_registry.data.exceptions import (UserRegistryDBDoesntExist,
                                           UserRegistryDBDuplicate,
                                           UserRegistryDBException)


def execute_db_query(fnct: Callable) -> Callable:
    """Decorator to execute SQL queries, returns UserRegistryDBException on error"""
    def wrapped(*args, **kwargs) -> LegacyCursorResult:

        try:
            return UserRegistryDataBase().execute(fnct(*args, **kwargs))
        except IntegrityError as err:
            raise UserRegistryDBDuplicate(f'Duplicated entry {str(err)}')
        except SQLAlchemyError as err:
            raise UserRegistryDBException(f'An error occured {str(err)}')
    return wrapped


def get_single_user_data(fnct: Callable) -> Callable:
    """Decorator to process data for single user data output,
       throwing UserRegistryDBDoesntExist or UserRegistryDBDuplicate in casse
    """
    def wrapped(username, *args, **kwargs) -> LegacyRow:
        user_data_cursor = fnct(username, *args, **kwargs)
        results = user_data_cursor.first()

        if not results:
            raise UserRegistryDBDoesntExist(f"User '{username}' not found")

        try:
            if user_data_cursor.first():
                raise UserRegistryDBDuplicate(f"Multiple rows returned for user '{username}' not found")
        except ResourceClosedError:
            pass

        return results
    return wrapped


@execute_db_query
def create_user(username: str, password: str) -> str:
    """Create a new ("unvalidated") user"""

    # NOTE: We are changing logging conditions LOCALLY, only for THIS transaction
    # This is in order not to let clear text passwords to appear in logs
    return (
        "SET LOCAL log_min_error_statement='PANIC';"
        "INSERT INTO user_accounts (EMAIL, PASSWORD) "
        f"VALUES ('{username}', sha512('{password}'));"
    )


@execute_db_query
def _get_user_data(username: str) -> str:
    """Raw data for user query"""

    return (
        "SELECT id, email, encode(password, 'hex') as password_hash, created_at, validated_at "
        "FROM user_accounts "
        f"WHERE email='{username}'"
    )


@get_single_user_data
def get_user(username: str) -> list[LegacyRow]:
    """Retrieve all details for a user"""
    return _get_user_data(username)


@execute_db_query
def _validate_user_data(username: str) -> str:
    """Set user as validated (by setting the VALIDATED_AT column)"""

    return (
        "UPDATE user_accounts "
        f"SET validated_at = NOW() "
        f"WHERE EMAIL = '{username}'"
    )


@execute_db_query
def add_validation_code(username: str, code: str) -> str:
    """Add a validation code for a particular user """

    return (
        "INSERT INTO validation_code (ACCOUNT_ID, VALIDATION_CODE) "
        f"  SELECT id, '{code}' "
        "   FROM user_accounts "
        f"  WHERE email = '{username}'"
    )


@execute_db_query
def verify_validation_code(username: str, code: str) -> str:
    """Add a validation code for a particular user """

    return (
        "UPDATE user_accounts "
        "SET validated_at = NOW() "
        "FROM validation_code "
        f"  WHERE user_accounts.email = '{username}' "
        "   AND validation_code.account_id = user_accounts.id "
        f"  AND validation_code.validation_code = '{code}' "
        f"  AND validation_code.created_at > NOW() - INTERVAL '{APP_NEW_ACCOUNT_VALIDATION_MINUTES} minute';"
    )
