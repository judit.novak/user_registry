import pytest

from user_registry import create_app
from user_registry.exceptions import UserRegistryException


def test_config():
    with pytest.raises(UserRegistryException):
        class BadConfig:
            SQLALCHEMY_DATABASE_URI = ""
        create_app(config=BadConfig)


def test_create_app(app):
    # NOTE: The app fixture is invoking create_app() with pointing to the test DB
    pass
