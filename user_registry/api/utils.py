from typing import Tuple, Any
from logging import Logger


def api_return(msg: str, code: int, logger: Logger = None) -> Tuple[dict, int]:
    if logger:
        logger.info(f"[API Response]: {str(msg)}")
    return {'message': str(msg)}, code


def api_error(msg_or_err: Any, code: int, logger: Logger = None) -> Tuple[dict, int]:
    if logger:
        logger.error(f"[API Error Response]: {str(msg_or_err)}")
    return {'message': str(msg_or_err)}, code
