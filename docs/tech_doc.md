﻿# Design and Technical Documentation

This document is providing insights to the project, both on an
architecture and on a technical level.

(Though normally Architectural Design and Technical aspects should rather be
explained in two documents, given the small size of the project we decided to
"squeeze" them both in one for now.)

## Background

While there is a wide range of tools and libraries out on the market
to facilitate classical tasks like this, one of the objectives of the
**user_registry** project was to move away from existing solutions
and provide an independent, self-containing solution.

We did out best to take advantage of native characteristics
aiming for a pythonic design, putting high focus on the choices
on objectives like

 - abstraction layers
 - separation of concerns
 - data flow
 - data encapsulation
 - structure of the project in terms of internal components/layers
 - communication across internal components via well-established interfaces

 of course overall most importantly reliability, efficiency and security.


## Dependencies

Within the highlights of above, critically relying on Python tools such as

 - mypy

And it is using no more than 

 - Flask
 - SQLAlchemy (**NO ORM**)
 
as dependencies.

As for the underlying database we choose

 - Postgres

## Design


We chose to follow similar base patterns as most Python web API frameworks.
However in order to focus on design objectives listed in the
[Background](#background) section, we decided to put a special focus on
internal data types, and static type check offered in Python.

This resulted in a project structure, where the higher the abstraction level
goes is the more we rely both default and project-specific, internal Data Class
and Data Model types to hide, encapsulate, validate and protect the data that
they carry across the workflows.

Additionally we made an attempt to keep the project structure following further
design aspects like a clear split between generic (re-usable) components from
others specific to the application (data, functionalities, etc.) itself.

Below we present a Design Diagram of the internal structure of the
**user_registry** project:

![user_registry Design Diagram](./UserRegistry-ArchitectureDiagram.png)


### Database Layer

In attempt to keep both the concept and the code clean, we are accessing the
data in multiple steps. While each piece of the chain offers a different level
of abstraction and complexity, they also allow for layers of better
understanding the nature of the data and to apply validation check on it.

#### Technicalities

The database schema is simple as lightweight. It contains two very simple tables:

 - one to store account data
 - one to store validation codes.

We use SQLAlchemy to provide base DB connection, with small, generic internal
wrapper class(es).  

Data is accessed an manipulated via direct SQL queries on the next level,
dealing with row data access.


### Models Layer

This layer is primarily responsible for data encapsulation and validation all in the
"shape of" internal abstraction. 

There are multiple supporting data types defined (Data Classes), each of
them responsible for the data "assigned", distributing logic and
functionalities.

Then the Abstract Model(s) (currently only a single one) is providing
functionalities that allow for the integration with UI Interface workflows.

Once all (critical) interactions happen via the manipulation of Data Type
objects, data validation methods will be applied natively ensuring safe data
flow across the project.

#### Technicalities

The Models Layer receives either raw data (SQLAlchemy result/row types), or
input from the User Interface level.

The main trick here is to use strong typing, and to benefit from
static checks provided by `mypy`, ensuring a level of integrity.

Ideally on top of static type checks we should also use dynamic type evaluation.
This could/should be a nice further extension to the project.

### Business Logic

Though the application contains minimal Business Logic, it's still the best to
explicitly separate that from the rest of the application code.

This layer is responsible for the orchestration of the actual workflows that are
associated with application functionalities.

### Technicalities

A main benefit of a clear Business Logic layer is to protect against the logic
being spread across the rest of the application code. Its a particularly
bad practice (resulting in un-manageable workflows) to have API endpoints or
Data Models "randomly" contain various chunks of the business logic.


### Interface Layer

This layer is mostly responsible for authentication, and invoking the
corresponding functionalities of the underlying Data Models.

Input data validation isn't directly implemented here, it is 
implicitly triggered instead. We are again taking advantage of our internal
data types, and their native, built-in validation. Thus input validation
is referring back to the Models Layer.

The interface layer consists of two levels. One to define the API endpoints,
perform basic checks, handle errors, etc. The lower layer is there to separate
small, straightforward execution flows (authentication steps, invoking Model
Layer functionalities with the right input, etc..) triggered by the endpoints.


#### Technicalities

Not much to mention here.


