from user_registry.external.communication_backend import send_code_to_communications_server


def test_send_code_to_communications_server(app, mock_external_comm_request):
    with app.app_context():
        assert send_code_to_communications_server('x@y.z', '1234')
